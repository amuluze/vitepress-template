/**
 * @Author     : Amu
 * @Date       : 2024/02/21 14:11:23
 * @Description:
 */
import { DefaultTheme } from "vitepress";
export const nav: DefaultTheme.NavItem[] = [
    {
        text: "基础入门",
        items: [
            {text: "Docker", link: "/views/docker/"},
        ]
    },
    {
        text: "实战案例",
        items: [
            {text: "Practice", link: "/views/practice/"}
        ]
    },
    {
        text: "关于我们",
        items: [
            {text: "关于我", link: "/views/about/"},
        ]
    },
]
