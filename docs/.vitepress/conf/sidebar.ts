/**
 * @Author     : Amu
 * @Date       : 2024/02/21 14:11:12
 * @Description:
 */
import { DefaultTheme } from 'vitepress';

import { dockerSidebarDetail } from './side';

export const sidebar: DefaultTheme.Sidebar = {
    ...dockerSidebarDetail
}
