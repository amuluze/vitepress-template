/**
 * docker - 测导详情
 */

export const dockerSidebarDetail = {
    '/views/docker/': [
        {
            text: 'docker',
            items: [
                {
                    text: 'docker 常用命令',
                    link: '/views/docker/docker/001_command'
                }
            ]
        },
        {
            text: 'docker-compose',
            items: [
                {
                    text: 'docker-compose 常用命令',
                    link: '/views/docker/docker-compose/001_compose_command'
                }
            ]
        }
    ]
}
