---
# https://vitepress.dev/reference/default-theme-home-page
layout: home
home: true

title: 阿慕路泽
head: [['link', { rel: 'icon', href: '/amuluze.ico' }]]
# titleTemplate: Hi,终于等到你！
editLine: true
lastUpdate: true

hero:
  name: "阿慕路泽"
  text: "路虽远，行则将至；\n事虽难，做则必成。"
  tagline: /独立开发者/破局三十五/工具控
  image:
    src: /amuluze.jpg
    alt: avatar
  actions:
    - theme: brand
      text: 进入主页
      link: /views/guide/

features:
  - icon: 🚎
    title: 独立开发者
    details: 试错中的入门独立开发者。
  - icon: 🍪
    title: 破局三十五
    details: 看我一个临“退”程序员如何破局？
  - icon: 🧰
    title: 关于我
    details: 生命不止，折腾不息。
    link: /views/about/
---
